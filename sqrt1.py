#!/usr/bin/env python

import numpy as np

a1 = -2
a2 = 2
b1 = -2
b2 = 2
Nr = 100
Ni = 100

x = np.linspace(a1,a2,Nr,endpoint=True)
y = np.linspace(b1,b2,Ni,endpoint=True)

fname="sqrt-complex.dat"
with open(fname,"w") as f:
    for i in range(Nr):
        for j in range(Ni):
            z = x[i]+1j*y[j]
            w = np.sqrt(z)
            f.write(f"{x[i]}\t{y[j]}\t{np.real(w)}\t{np.imag(w)}\t{np.angle(w)}\n")
