#!/usr/bin/env python

import numpy as np

a1 = -5
a2 = 100
b1 = -100
b2 = 100
Nr = 200
Ni = 200
U0=5
a=1

x = np.linspace(a1,a2,Nr,endpoint=True)
y = np.linspace(b1,b2,Ni,endpoint=True)

fname="amp.dat"
with open(fname,"w") as f:
    for i in range(Nr):
        for j in range(Ni):
            E = x[i]+1j*y[j]
            k1=np.sqrt(U0+E)
            k=np.sqrt(E)
            w = 1/(2*np.cos(a*k1)-1j*np.sin(a*k1)*(k1/k+k/k))*np.exp(-1j*k*a)
            f.write(f"{x[i]}\t{y[j]}\t{np.real(w)}\t{np.imag(w)}\t{np.angle(w)}\n")
