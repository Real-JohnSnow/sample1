#!/usr/bin/env python

import numpy as np

a1 = -5
a2 = 10
b1 = -2
b2 = 2
Nr = 200
Ni = 200
U0=5
a=3

def aD(E):
    k1 = np.sqrt(U0+E)
    k = np.sqrt(E)
    return 1/(2*np.cos(a*k1)-1j*np.sin(a*k1)*(k1/k+k/k))*np.exp(-1j*k*a)

def aC1(E):
    k1 = np.sqrt(U0+E)
    k = np.sqrt(E)
    return aD(E)*(1+k/k1)*np.exp(1j*a*(k-k1))

def aC2(E):
    k1 = np.sqrt(U0+E)
    k = np.sqrt(E)
    return aD(E)*(1-k/k1)*np.exp(1j*a*(k+k1))

def aB(E):
    k1 = np.sqrt(U0+E)
    k = np.sqrt(E)
    return aD(E)*np.exp(1j*k*a)*1j*np.sin(k1*a)*(k1/k-k/k1)

def psi1(E,x):
    k1 = np.sqrt(U0+E)
    k = np.sqrt(E)
    w=np.exp(1j*k*x)+aB(E)*np.exp(-1j*k*x)
    return np.abs(w)**2

def psi2(E,x):
    k1 = np.sqrt(U0+E)
    k = np.sqrt(E)
    w=aC1(E)*np.exp(1j*k1*x)+aC2(E)*np.exp(-1j*k1*x)
    return np.abs(w)**2

def psi3(E,x):
    k1 = np.sqrt(U0+E)
    k = np.sqrt(E)
    w=aD(E)*np.exp(1j*k*x)
    return np.abs(w)**2

def psi(E,x):
    r1 = np.zeros_like(x)
    r2 = np.zeros_like(x)
    r3 = np.zeros_like(x)
    r1[x<0] = 1.
    r3[x>a] = 1.
    r2[(x<a) & (x>0)] = 1.
    return r1*psi1(E,x)+r2*psi2(E,x)+r3*psi3(E,x)
